# .bashrc

# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.
if [[ $- != *i* ]] ; then
	# Shell is non-interactive.  Be done now!
	return
fi

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific aliases and functions

# setup some aliases
alias d="ls --color"
alias ls="ls --color=auto"
alias ll="ls --color -l"
alias la="ls --color -a"
alias lart="ls --color -lart"
alias larth="ls --color -larth"
alias arin="whois -h whois.arin.net"
alias ripe="whois -h whois.ripe.net"

alias get-yum="get-yum-history.sh -k ~/.ssh/pang_id_rsa"

alias list-vpc="aws ec2 describe-vpcs --query \"Vpcs[].{vpcid:VpcId,name:Tags[?Key=='Name'].Value,cidr:CidrBlock}\""

# set PATH to include bin in the user's home directory
PATH="/home/${USER}/bin:${PATH}:/usr/sbin/:/sbin"
export PATH

# use vi as my EDITOR
export EDITOR="vim"

# make terragrunt always update source
export TERRAGRUNT_SOURCE_UPDATE=true

# set path to composter home
# this is used by composer to find credentials to pull from artifactory
export COMPOSER_HOME="/usr/local/etc/composer"

# Change the window title of X terminals 
case ${TERM} in
	xterm*|rxvt*|Eterm|aterm|kterm|gnome)
		PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/$HOME/~}\007"'
		;;
	screen)
		PROMPT_COMMAND='echo -ne "\033_${USER}@${HOSTNAME%%.*}:${PWD/$HOME/~}\033\\"'
		;;
esac

# We only want the git prompt info in directories that are git repos
GIT_PROMPT_ONLY_IN_REPO=1

# Grab the local colorful bash prompt setting
[ -f /etc/profile.d/bash_prompt ] && source /etc/profile.d/bash_prompt

# configure git for us
[ -f /etc/profile.d/set-git-info ] && source /etc/profile.d/set-git-info


# locale settings
# http://www.gentoo.org/doc/en/guide-localization.xml
# also see http://www.linuxquestions.org/questions/slackware-14/gnome-clock-411886/
export LC_ALL="en_US.UTF-8"
export LANG="en_US.UTF-8"


# enable terraform logging
# https://www.terraform.io/docs/internals/debugging.html
alias tflog='export TF_LOG=INFO && export TF_LOG_PATH=terraform.log'


# this function lets you set which profile to use
# from ~/.aws/credentials
# it also updates your prompt to remind you which aws profile you are using
function setaws() {
    if [ -f /etc/profile.d/bash_prompt ]
    then
        source /etc/profile.d/bash_prompt
    fi
    
    if [ "$1" == "somerandom_id" ]
    then
        # set our region at the same time
        export AWS_DEFAULT_REGION="us-west-5"
    fi

    export AWS_DEFAULT_PROFILE="$1"
    export AWS_PROFILE="$1"

    export PS1="\[\033[01;37m\][AWS]\[\033[01;31m\]:\[\033[01;32m\][$AWS_DEFAULT_PROFILE]\[\033[01;31m\]:\[\033[01;32m\][$AWS_DEFAULT_REGION]\n\[\033[01;33m\]$PS1"
    export GIT_PROMPT_START="\[\033[01;37m\][AWS]\[\033[01;31m\]:\[\033[01;32m\][$AWS_DEFAULT_PROFILE]\[\033[01;31m\]:\[\033[01;32m\][$AWS_DEFAULT_REGION]"
}

# quickly set which aws region to use
# also updates your prompt
function setregion() {
    if [ -f /etc/profile.d/bash_prompt ]
    then
        source /etc/profile.d/bash_prompt
    fi

    export AWS_DEFAULT_REGION="$1"
    export PS1="\[\033[01;37m\][AWS]\[\033[01;31m\]:\[\033[01;32m\][$AWS_DEFAULT_PROFILE]\[\033[01;31m\]:\[\033[01;32m\][$AWS_DEFAULT_REGION]\n\[\033[01;33m\]$PS1"
    export GIT_PROMPT_START="\[\033[01;37m\][AWS]\[\033[01;31m\]:\[\033[01;32m\][$AWS_DEFAULT_PROFILE]\[\033[01;31m\]:\[\033[01;32m\][$AWS_DEFAULT_REGION]"
}

# switch terraform and terragrunt to mednet versions
function mednet-terraform() {
    rm ~/bin/terraform
    rm ~/bin/terragrunt
    ln -s ~/bin/terraform_v0.9.11 ~/bin/terraform
    ln -s ~/bin/terragrunt_linux_amd64.v0.12.24 ~/bin/terragrunt
}

# switch to latest terraform and terragrunt
function latest-terraform {
    rm ~/bin/terraform
    rm ~/bin/terragrunt
    ln -s $( ls -rt ~/bin/terragrunt_linux* | tail -n 1 ) ~/bin/terragrunt
    ln -s $( ls -rt ~/bin/terraform_v* | tail -n 1 ) ~/bin/terraform
}

